/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.ftcfiller;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.HardwareMap;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

/**
 * Class used for automatically "filling" hardware fields in an {@link OpMode}
 */
public class Filler {
    public static final Map<Class<?>, Field> hardware = new HashMap<>();

    static {
        Field[] fields = HardwareMap.class.getFields();
        for (Field field : fields) {
            Class<?> clazz = field.getType();
            if (HardwareMap.DeviceMapping.class.isAssignableFrom(clazz)) {
                hardware.put((Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0], field);
            }
        }
    }

    /**
     * "Fills" all fields annotated with {@link FillByName} in the {@link OpMode}. Objects are
     * retrieved from the corresponding {@link HardwareMap.DeviceMapping} using the variable name
     * @param opMode the {@link OpMode} whose annotated fields are to be "filled"
     */
    public static void fill(OpMode opMode) {
        Field[] fields = opMode.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(FillByName.class)) {
                Class<?> clazz = field.getType();
                String name = field.getName();
                Object device = null;

                for (Map.Entry<Class<?>, Field> e : hardware.entrySet()) {
                    if (e.getKey().isAssignableFrom(clazz)) { //if clazz is the same as or a subclass of the class in this entry (assumes downcast is possible)
                        try {
                            device = ((HardwareMap.DeviceMapping<?>) e.getValue().get(opMode.hardwareMap)).get(name);
                        } catch (IllegalAccessException ignored) {}
                        break;
                    }
                }

                if (device != null) {
                    field.setAccessible(true);
                    try {
                        field.set(opMode, device);
                    } catch (IllegalAccessException ignored) {}
                }
            }
        }
    }
}
