/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.ftcfiller;

import android.support.annotation.CallSuper;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * {@link OpMode} that automatically "fills" its annotated fields
 */
public abstract class FillingOpMode extends OpMode {
    /**
     * Calls {@link Filler#fill(OpMode)} on {@code this} for you. Subclasses must call this override
     * of {@link OpMode#init()} when further overriding it. Subclasses that do need any further
     * initialization can safely skip overriding this method.
     */
    @CallSuper
    @Override
    public void init() {
        Filler.fill(this);
    }
}
